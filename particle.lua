require 'utils'

Particle = {}
Particle_mt = { __index = Particle }
function Particle:new(x,y, velocity, movement, lifespan)
    local particle = {
        x=x, y=y,
        velocity = velocity or 0,
        movement = movement,
        lifespan = lifespan or 0,
        color=love.math.random()
    }
    setmetatable(particle, Particle_mt)
    return particle
end

-- between 0.75 & 0.9
function Particle:max_light()
    return self.color % 0.15 + 0.75
end

function Particle:max_satura()
    return self.color % 0.15 + 0.75
end

function Particle:update(movement, dt)
    if movement.kind == 'target' then
        return self:move_toward(movement.target, dt)
    elseif movement.kind == 'linear' then
        return self:move(dt)
    end
end

function Particle:move(dt)
    local x = self.x + self.movement.x * self.velocity * dt
    local y = self.y + self.movement.y * self.velocity * dt
    local particle = {
        x=x, y=y,
        velocity = math.min(self.velocity + 100 * dt, 500),
        lifespan = self.lifespan + dt/2,
        color = self.color,
        movement = self.movement,
    }
    setmetatable(particle, Particle_mt)
    return particle
end

function Particle:move_toward(target, dt)
    local delta = move_toward(self, target, self.velocity * dt)
    local particle = {
        x = self.x + delta.x,
        y = self.y + delta.y,
        velocity = math.min(self.velocity + 10 * dt, 1000),
        lifespan = self.lifespan + dt,
        color = self.color,
        movement = self.movement,
    }
    setmetatable(particle, Particle_mt)
    return particle
end
