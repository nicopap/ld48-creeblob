class = require 'HC.class'
Polygon = require 'HC.polygon'
require 'utils'
HC = require 'HC'

Player = {}
Player_mt = { __index = Player }
function Player:new(x,y)
    local player = {
        x=x, y=y, velocity=0,
        collision = HC.polygon(Polygon(circle_at(x,y))),
        sprite = love.graphics.newCanvas(1000,1000),
        size = 0,
    }
    setmetatable(player, Player_mt)
    return player
end

-- dt: number
-- movement: { x_dir, y_dir }
function Player:update(dt, movement)
    local x = self.x + movement.x_dir * self.velocity * dt
    local y = self.y + movement.y_dir * self.velocity * dt
    self.collision:moveTo(x,y)
    local player = {
        x=x, y=y,
        velocity = math.min(self.velocity + 100 * dt, 500),
        collision = self.collision,
        sprite = self.sprite,
        size = self.size
    }
    setmetatable(player, Player_mt)
    return player
end

function Player:contains(position)
    return self.collision:contains(position.x, position.y)
end

function Player:glob_particle(particle)
    local dx = self.x - particle.x
    local dy = self.y - particle.y
    local angle = math.atan(math.abs(dy) / math.abs(dx))

    local vs = self.collision._polygon.vertices
    local closest = closest_angular(vs, self, particle)
    closest.x = closest.x + math.cos(angle) * ((dx > 0) and -1 or 1)
    closest.y = closest.y + math.sin(angle) * ((dy > 0) and -1 or 1)
    self.size = self.size + 1

    local canvas_x = closest.x - self.x + 500
    local canvas_y = closest.y - self.y + 500
    local s = math.min(particle:max_satura(), particle.lifespan / 5)
    local l = math.min(particle:max_light(), particle.lifespan / 5)
    local r,g,b = HSL(particle.color, s, l, 1)
    love.graphics.setCanvas(self.sprite)
    love.graphics.setColor(r,g,b,1)
    love.graphics.points({{canvas_x, canvas_y}})
    love.graphics.setColor(1,1,1,1)
    love.graphics.setCanvas()

    return self
end
