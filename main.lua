update = require 'game.update'
view = require 'game.view'
require 'player'
require 'utils'

graphics = {}
world = {
    player = Player:new(300, 100),
    enemies = {},
    cursor = { x = 200, y = 200 },
    player_particles = {},
    cloud_particles = {},
    t = 0,
}

function love.load()
    love.window.setMode(1420, 1000)
    local font = love.graphics.newFont("ssp-bold.ttf", 30)
    love.graphics.setFont(font)

    graphics.cursor = love.graphics.newImage("cursor.png")
end

function love.update(dt)
    world.t = world.t + dt

    local joysticks = love.joystick.getJoysticks()
    local joystick = nil
    if #joysticks > 0 then joystick = joysticks[1] else joystick = nil
    end

    local controls = update.controls(joystick)
    local updated = update.game(world, controls, dt)
    world.cursor = updated.cursor
    world.player_particles = updated.player_particles
    world.cloud_particles = updated.cloud_particles
    world.player = updated.player
    world.enemies = updated.enemies
    world.death_event = updated.death_event
    world.t = updated.t
end

function love.draw()
    view.draw(graphics, world)
end

