class = require 'HC.class'
Polygon = require 'HC.polygon'
require 'particle'
require 'utils'
HC = require 'HC'

Enemy = {}
Enemy_mt = { __index = Enemy }

function Enemy:new(x,y, size, momentum)
    local blob = Polygon(blob_at(x,y, size, size * 4))
    local color = (love.math.random() * 0.15 - 0.075) % 0.15
    local enemy = {
        collision = HC.polygon(blob),
        color=love.math.random(),
        hp = math.pow(size * 20, 1.5),
        lifespan = 100,
        momentum = momentum,
        outline = HC.polygon(blob),
        size = size,
        sprite = love.graphics.newCanvas(200,200),
        velocity = math.min(0, 700 - math.pow(size * 20, 1.2)),
        x=x, y=y,
    }
    setmetatable(enemy, Enemy_mt)
    return enemy
end

function Enemy:update(target, dt)
    local delta = move_toward(self, target, self.velocity * 3)
    local momentum = {
        x = (self.momentum.x*0.95) + (delta.x*0.05),
        y = (self.momentum.y*0.95) + (delta.y*0.05),
    }
    local x = self.x + dt * momentum.x
    local y = self.y + dt * momentum.y
    self.collision:moveTo(x,y)
    self.outline:moveTo(x,y)
    local enemy = {
        collision = self.collision,
        color = self.color,
        hp = self.hp,
        lifespan = self.lifespan - dt * 10,
        momentum = momentum,
        outline = self.outline,
        size = self.size,
        sprite = self.sprite,
        velocity = math.min(self.velocity + dt * 10, 100),
        x=x, y=y,
    }
    setmetatable(enemy, Enemy_mt)
    return enemy
end

function Enemy:contains(position)
    return self.collision:contains(position.x, position.y)
end

function Enemy:glob_particle(particle)
    local dx = self.x - particle.x
    local dy = self.y - particle.y
    local angle = math.atan(math.abs(dy) / math.abs(dx))

    local vs = self.collision._polygon.vertices
    local closest = closest_angular(vs, self, particle)
    closest.x = closest.x + math.cos(angle) * ((dx > 0) and -1 or 1)
    closest.y = closest.y + math.sin(angle) * ((dy > 0) and -1 or 1)

    local canvas_x = closest.x - self.x + 100
    local canvas_y = closest.y - self.y + 100
    local s = math.min(particle:max_satura(), particle.lifespan / 5)
    local l = math.min(particle:max_light(), particle.lifespan / 5)
    local r,g,b = HSL(particle.color, s, l, 1)
    love.graphics.setCanvas(self.sprite)
    love.graphics.setColor(r,g,b,1)
    love.graphics.points({{canvas_x, canvas_y}})
    love.graphics.setColor(1,1,1,1)
    love.graphics.setCanvas()

    self.hp = self.hp - 1
    return self
end

function Enemy:explode(plist)
    local x_center = self.x
    local y_center = self.y
    for i = 1,math.pow(self.size,2)*100 do
        local angle = love.math.random() * math.pi * 2
        local dist = love.math.random() * self.size
        plist[#plist+1] = Particle:new(
            x_center + math.sin(angle) * dist * love.math.random(),
            y_center + math.cos(angle) * dist* love.math.random(),
            self.velocity * 2,
            { x = love.math.random() - 0.5, y = love.math.random() - 0.5 },
            self.lifespan / 10
        )
    end
end
function Enemy:derez(plist)
    local x_center = self.x
    local y_center = self.y
    local x_dir = self.momentum.x / 100
    local y_dir = self.momentum.y / 100
    for i = 1,math.pow(self.size,2)*100 do
        local angle = love.math.random() * math.pi * 2
        local dist = love.math.random() * self.size
        plist[#plist+1] = Particle:new(
            x_center + math.sin(angle) * dist * love.math.random(),
            y_center + math.cos(angle) * dist * love.math.random(),
            self.velocity,
            {
                x = x_dir + love.math.random() * 0.3,
                y = y_dir + love.math.random() * 0.3
            }
        )
    end
end
