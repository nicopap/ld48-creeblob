fun = require 'fun'
require 'utils'

local view = {}

local instruction_timeout = 25
local death_fadein = 5

function view.draw(graphics, world)
    function ppToLovePart(pp)
        local s = math.min(pp:max_satura(), pp.lifespan / 3)
        local l = math.min(pp:max_light(), pp.lifespan / 3)
        local r,g,b,a = HSL(pp.color, s, l, 1)
        love.graphics.setColor(r,g,b,a)
        return {pp.x, pp.y}
    end
    if world.t < title_timeout then
        love.graphics.setColor(
            1,1,1, 1 - world.t/title_timeout
        )
        love.graphics.printf("Blobula ex nihili", 100, 400, 300)
        love.graphics.setColor(1,1,1)
    elseif world.death_event then
        local death_event = world.death_event
        function opacity(offset) return
            math.min(1, (world.t-death_event-offset)/death_fadein)
        end
        love.graphics.setColor(1,1,1, opacity(0))
        love.graphics.printf("You have been hit,", 100, 100, 1000, 'left')

        love.graphics.setColor(1,1,1, opacity(3))
        love.graphics.printf("the game is over.", 100, 160, 1000, 'left')

        love.graphics.setColor(1,1,1, opacity(6))
        love.graphics.printf("You accrued", 100, 240, 1000, 'left')

        love.graphics.setColor(0.3,0.3,0.6, opacity(1))
        love.graphics.printf(world.player.size, 275, 240, 1000, 'left')

        love.graphics.setColor(1,1,1, opacity(9))
        love.graphics.printf("pixels", 350, 240, 1000, 'left')

        love.graphics.setColor(1,1,1, opacity(11))
        love.graphics.printf("and survived for", 100, 300, 1000, 'left')

        love.graphics.setColor(0.6,0.3,0.3, opacity(2))
        love.graphics.printf(math.ceil(world.death_event), 330, 300, 1000, 'left')

        love.graphics.setColor(1,1,1, opacity(12.5))
        love.graphics.printf("seconds.", 390, 300, 1000, 'left')

        love.graphics.setColor(0.2,0.2,0.2, opacity(16))
        love.graphics.printf("(r/start to restart)", 370, 340, 1000, 'left')
    else
        if world.t < instruction_timeout + title_timeout then
            local opacity =0.4 - (world.t-title_timeout)/instruction_timeout
            love.graphics.setColor( 1,1,1, opacity)
            love.graphics.printf("Press", 100, 100, 300)

            love.graphics.setColor(0.6,0.6,0.6, opacity)
            love.graphics.printf("Left trigger/Space", 180, 100, 300)
            love.graphics.setColor(1,1,1)
        end
        love.graphics.draw(graphics.cursor, world.cursor.x - 4, world.cursor.y - 4)
        for i,e in ipairs(world.enemies) do
            local life = e.lifespan / 330
            local health = (world.t % (e.hp)) / e.hp
            local r,g,b,a = HSL(e.color, health, (life*0.8 + health*0.2), 1)
            love.graphics.setColor(r,g,b)
            e.outline:draw('fill')
            love.graphics.setColor(1,1,1)
            love.graphics.draw(e.sprite, e.x - 100, e.y - 100)
        end
        love.graphics.setColor(1,1,1)
        love.graphics.draw(
            world.player.sprite,
            world.player.x - 500,
            world.player.y - 500
        )
        local x =
            fun.map(ppToLovePart, world.player_particles)
                :chain(fun.map(ppToLovePart, world.cloud_particles))
        fun.each(love.graphics.points, x)
        love.graphics.setColor(1,1,1)
    end
end

return view
