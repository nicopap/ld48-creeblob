fun = require 'fun'
require 'utils'
require 'particle'
require 'enemey'

local update = {}

local movement_speed = 234
local win_x = 1420
local win_y = 1000

function update_target(dt, target, movement)
    return {
        x = target.x + movement.x_dir * dt * movement_speed,
        y = target.y + movement.y_dir * dt * movement_speed
    }
end

function update.game(world, controls, dt)
    if world.death_event then
        if controls.reset == true then
            return {
                player = Player:new(300, 100),
                enemies = {},
                cursor = { x = 200, y = 200 },
                player_particles = {},
                cloud_particles = {},
                t = title_timeout,
            }
        else
            world.t = world.t + dt
            return world
        end
    end

    local curs_move = {
        x_dir = controls.curs_x_dir,
        y_dir = controls.curs_y_dir
    }
    local cursor = update_target(dt, world.cursor, curs_move)
    local player = world.player:update(dt, controls)
    function nppart_updt(part) return part:update({kind='linear'}, dt) end
    function ppart_updt(part) return
        part:update({kind='target', target=player}, dt)
    end
    function enm_updt(enm) return enm:update(player, dt) end
    function player_eat(part)
        if player:contains(part) then
            player:glob_particle(part)
            return false
        else return true end
    end
    function part_in(part) return
        not (part.x < 0 or part.y < 0 or part.x > win_x or part.y > win_y)
    end
    function glob(part) player:glob_particle(part) end


    local pp = world.player_particles
    local npp = world.cloud_particles
    local e = world.enemies
    if controls.action == 1 then
        pp[#pp + 1] = Particle:new(cursor.x, cursor.y)
    end
    local frame_rand = (world.t + love.math.random()) % 1
    if frame_rand < world.player.size * 1e-5 then
        local spawn_rand = (world.t + love.math.random()) % 1
        local spawn =
            (spawn_rand < 0.25)
                and {
                    x=0, y=love.math.random() * win_y,
                    x_dir=100, y_dir=0
            } or (spawn_rand < 0.5)
                and {
                    x=win_x, y=love.math.random() * win_y,
                    x_dir=-100, y_dir=0
            } or (spawn_rand < 0.75)
                and {
                    x=love.math.random() * win_x, y=0,
                    x_dir=0, y_dir=100
            } or {
                x=love.math.random() * win_x, y=win_y,
                x_dir=0, y_dir=-100,
            }
        local size_variance = math.max(0.01,1 + love.math.randomNormal(0.3))
        local spawn_size = player.size * size_variance / 1000

        e[#e+1] = Enemy:new(
            spawn.x, spawn.y, spawn_size,
            {x=spawn.x_dir, y=spawn.y_dir}
        )
    end

    function enemy_eat(part)
        for i, enm in ipairs(e) do if enm:contains(part) then
            enm:glob_particle(part); return false
        end end
        return true
    end
    function enm_in(enm)
        if enm.lifespan < 0
        then enm:derez(npp); return false
        elseif enm.hp < 0
        then enm:explode(npp); return false
        else return true
        end
    end
    function enm_touches(enm) return
        enm.collision:collidesWith(player.collision)
    end
    e = fun.filter(enm_in, e):map(enm_updt)
    local player_touched = e:any(enm_touches)
    e = e:totable()
    npp = fun.filter(part_in, npp):map(nppart_updt):filter(player_eat)
    pp = fun.filter(part_in, pp)
        :map(ppart_updt)
        :filter(player_eat)
        :filter(enemy_eat)

    return {
        cursor = cursor,
        player_particles = pp:totable(),
        cloud_particles = npp:totable(),
        enemies = e,
        player = player,
        t = world.t + dt,
        death_event = player_touched and world.t or nil,
    }
end

function update.controls(joystick)
    function joy_cursor(axis)
        if joystick then
            local a1, a2, a3, a4, a5 = joystick:getAxes()
            if axis == 'x1' then return a1
            elseif axis == 'y1' then return a2
            elseif axis == 'ltrigger' then return a3
            elseif axis == 'x2' then return a4
            elseif axis == 'y2' then return a5
            end
        end
        return 0
    end
    function direction(pos, neg)
        if love.keyboard.isDown(pos) then return 1
        elseif love.keyboard.isDown(neg) then return -1
        else return 0
        end
    end

    local a_down = joystick and joy_cursor('ltrigger') > 0.9
    local reset_down = joystick and joystick:isGamepadDown('start')

    return {
        action = (love.keyboard.isDown("space") or a_down) and 1 or 0,
        reset = love.keyboard.isDown("r") or reset_down,
        x_dir = direction('d','a') + joy_cursor('x1'),
        y_dir = direction('s','w') + joy_cursor('y1'),
        curs_x_dir = direction('right','left') + joy_cursor('x2'),
        curs_y_dir = direction('down','up') + joy_cursor('y2'),
    }
end

return update
