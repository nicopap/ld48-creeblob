# Blobula ex nihili

My LD45 comp entry.

It is written in lua and uses the löve2d game engine.
I use a patched version

# Building the game

* Install [löve](https://love2d.org)
* copy the lua.fun file from [the luafun lib](https://github.com/luafun/luafun) in the root of the repository
* Make sure the HC git submodule was fetched (I use a patched version of the
  library, because it was broken)
* `cd ld48-creeblob`
* `love .`

# The game itself

It's like a twin-stick shooter, but instead of shooting toward the cursor, the
cursor shoots toward you (this was accidental, I just found the idea of having
the pixels blob together nice)
