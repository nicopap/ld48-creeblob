fun = require "fun"
title_timeout = 3

-- Converts HSL to RGB. (input and output range: 0 - 255)
function HSL(h, s, l, a)
    if s<=0 then return l,l,l,a end
    h = h * 6
    s = s * (256 / 255)
    l = l * (256 / 255)
    local c = (1-math.abs(2*l-1))*s
    local x = (1-math.abs(h%2-1))*c
    local m,r,g,b = (l-.5*c), 0,0,0
    if h < 1     then r,g,b = c,x,0
    elseif h < 2 then r,g,b = x,c,0
    elseif h < 3 then r,g,b = 0,c,x
    elseif h < 4 then r,g,b = 0,x,c
    elseif h < 5 then r,g,b = x,0,c
    else              r,g,b = c,0,x
    end return (r+m),(g+m),(b+m),a
end

function move_toward(from, to, speed)
    local dx = from.x - to.x
    local dy = from.y - to.y
    local angle = math.atan(math.abs(dy) / math.abs(dx))
    return {
        x = speed * math.cos(angle) * ((dx > 0) and -1 or 1),
        y = speed * math.sin(angle) * ((dy > 0) and -1 or 1)
    }
end

function build_table(generator)
    local t = {}
    function add_entry(entry)
        t[#t+1] = entry
    end
    fun.each(add_entry, generator)
    return t
end

function circle_at(x, y)
    local t = {}
    fun.range(0, 2 * math.pi, 0.02):each(function(angle)
        t[#t+1] = (x + math.sin(angle)) * 0.1
        t[#t+1] = (y + math.cos(angle)) * 0.1
    end)
    return t
end

function closest_angular(shape, center, to)
    local min_dist, min_point = math.huge, {}
    for i,p in ipairs(shape) do
        local pdx = center.x - p.x
        local pdy = center.y - p.y
        local tdx = center.x - to.x
        local tdy = center.y - to.y
        local quadrant =
            (tdy > 0 and pdy > 0 and tdx > 0 and pdx > 0) and 0.0
            or (tdy > 0 and pdy > 0 and tdx < 0 and pdx < 0) and math.pi * 0.5
            or (tdy < 0 and pdy < 0 and tdx < 0 and pdx < 0) and math.pi
            or (tdy < 0 and pdy < 0 and tdx > 0 and pdx > 0) and math.pi * 1.5
            or nil
        if quadrant ~= nil then
            local dist = math.abs(math.atan(pdx / pdy) - math.atan(tdx / tdy))
            if dist < min_dist then
                min_dist = dist
                min_point = p
            end
        end
    end
    return min_point
end

function  blob_at(x, y, radius, bump_ratio)
    bump_ratio = bump_ratio or 1
    local t = {}
    local xs = {}
    for i = 1,180 do xs[i] = math.abs(love.math.randomNormal(2, 0)) * 3 end
    function xavg(center)
        return fun.range(10)
            :map(function(i) return xs[(i - center) % #xs + 1]end)
            :sum() / 10
    end
    local i = 1
    for angle = 0, 2 * math.pi, 0.1 do
        local bump = xavg(i)
        t[#t+1] = (x + math.sin(angle) * (radius + bump*bump_ratio))
        t[#t+1] = (y + math.cos(angle) * (radius + bump*bump_ratio))
        i = i + 1
    end
    return t
end
